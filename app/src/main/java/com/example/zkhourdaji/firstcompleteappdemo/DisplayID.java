package com.example.zkhourdaji.firstcompleteappdemo;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;
import com.adtapsy.sdk.*;

import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by zkhourdaji on 2/8/16.
 */
public class DisplayID extends AppCompatActivity {
    ImageView ourImageView;
    int width, height, id_width;
    double id_height, selfie_width, selfie_height;
    Bitmap scaledDownTemplate;
    final int SHARE_CODE = 1;

    //Ads
    boolean hasShown = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.displayid_layout);
        ourImageView = (ImageView) findViewById(R.id.imageView);
        Bundle b = getIntent().getExtras();
        setSizes();
        drawIDWithPicture((Uri) b.get("picture"));

        //Handle Analytics reporting of ads
        //Handle other stuff
        AdTapsy.setDelegate(new AdTapsyDelegate() {
            @Override
            public void onAdClicked(int i) {

            }

            @Override
            public void onAdShown(int i) {

            }

            @Override
            public void onAdFailToShow(int i) {

            }

            @Override
            public void onAdCached(int i) {

            }

            @Override
            public void onAdSkipped(int i) {

            }
        });

    }



    @Override
    public void onStart() {
        super.onStart();
        AdTapsy.onStart(this);
        callAdOnce();
    }

    public void callAdOnce() {
        if(!hasShown)
        {
            if(AdTapsy.isInterstitialReadyToShow()) {
                AdTapsy.showInterstitial(this);
                hasShown = true;
            }
        }
    }

    //AdTapsy Required
    @Override
    public void onResume() {
        super.onResume();
        AdTapsy.onResume(this);
    }

    //AdTapsy Required
    @Override
    public void onPause() {
        super.onPause();
        AdTapsy.onPause(this);
    }

    //AdTapsy Required
    @Override
    public void onStop() {
        super.onStop();
        AdTapsy.onStop(this);
    }

    //AdTapsy Required
    @Override
    public void onDestroy() {
        super.onDestroy();
        AdTapsy.onDestroy(this);
    }

    //AdTapsy Required
    @Override
    public void onBackPressed() {
        // If ad is on the screen - close it
        if(!AdTapsy.closeAd()){
            super.onBackPressed();
        }
    }

    private void drawIDWithPicture(Uri pictureUri) {
        try {
            scaledDownTemplate = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(
                    getResources(), R.drawable.mclovin_id), id_width, (int) id_height, true);
            Canvas canvas = new Canvas(scaledDownTemplate);
            ourImageView.setImageBitmap(scaledDownTemplate);
            Bitmap scaledDownPicture = Bitmap.createScaledBitmap(MediaStore.Images.Media.getBitmap(
                    this.getContentResolver(), pictureUri), (int) selfie_width, (int) selfie_height, true);
            canvas.drawBitmap(NeededStaticMethods.rotateBitmap(scaledDownPicture, NeededStaticMethods.totalRotation), 0.001f, 0.001f, new Paint());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void setSizes() {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        height = size.y;
        id_width = width;
        id_height = id_width / 1.8;
        selfie_width = id_width / 2.7;
        selfie_height = id_height / 1.5;
    }


    public void displayButtonListener(View view) {
        if (view.getId() == R.id.shareButton){
            final Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("image/*");
            shareIntent.putExtra(Intent.EXTRA_STREAM, NeededStaticMethods.getImageUri(this, scaledDownTemplate));
            startActivity(Intent.createChooser(shareIntent, "Share image using"));
        }
        else if (view.getId() == R.id.saveButton){
                Toast.makeText(getApplicationContext(), "Save not implemented yet", Toast.LENGTH_SHORT).show();

            }
        }
    }



