package com.example.zkhourdaji.firstcompleteappdemo;

import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.adtapsy.sdk.AdTapsy;
import java.io.IOException;

public class MainActivity extends AppCompatActivity{
    private final int UPLOAD_PICTURE = 2, TAKE_PICTURE = 1;

    //AdTapsy Required
    private final String adAppID = "56c4c36ae4b0d72cf1bbf42b";

    public Intent moveToDisplay;
    Uri mcLovinFaceUri;
    ImageView previewImage;
    Bitmap imageBitmap = null;
    boolean userProvidedImage = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //AdTapsy Required
        AdTapsy.onCreate(this);
        AdTapsy.startSession(this, adAppID);

        setContentView(R.layout.activity_main);
        previewImage = (ImageView)findViewById(R.id.imagePreview);
        moveToDisplay = new Intent(getApplicationContext(), DisplayID.class);
        mcLovinFaceUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" +
                getResources().getResourcePackageName(R.drawable.mclovin_face) + '/' +
                getResources().getResourceTypeName(R.drawable.mclovin_face) + '/' +
                getResources().getResourceEntryName(R.drawable.mclovin_face) );
        previewImage.setImageURI(mcLovinFaceUri);
        imageBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.mclovin_face);

    }

    //AdTapsy Required
    @Override
    protected void onStart() {
        super.onStart();
        AdTapsy.onStart(this);
    }

    //AdTapsy Required
    @Override
    public void onResume() {
        super.onResume();
        AdTapsy.onResume(this);
    }

    //AdTapsy Required
    @Override
    public void onPause() {
        super.onPause();
        AdTapsy.onPause(this);
    }

    //AdTapsy Required
    @Override
    public void onStop() {
        super.onStop();
        AdTapsy.onStop(this);
    }

    //AdTapsy Required
    @Override
    public void onDestroy() {
        super.onDestroy();
        AdTapsy.onDestroy(this);
    }

    //AdTapsy Required
    @Override
    public void onBackPressed() {
        // If ad is on the screen - close it
        if(!AdTapsy.closeAd()){
            super.onBackPressed();
        }
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(resultCode == RESULT_OK) {
            userProvidedImage = true;
            try {
                imageBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), data.getData());
                previewImage.setImageBitmap(imageBitmap);
                if(requestCode == TAKE_PICTURE) {
                    moveToDisplay.putExtra("picture", data.getData());
                }
                else if(requestCode == UPLOAD_PICTURE){
                    moveToDisplay.putExtra("picture", data.getData());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void buttonsListener(View view){
        if (view.getId() == R.id.clockWise){
            previewImage.setImageBitmap(imageBitmap = NeededStaticMethods.rotateBitmap(imageBitmap, 90));
            NeededStaticMethods.totalRotation += 90;
        }
        else if (view.getId() == R.id.counterCLockWise){
            previewImage.setImageBitmap(imageBitmap = NeededStaticMethods.rotateBitmap(imageBitmap, -90));
            NeededStaticMethods.totalRotation -= 90;
        }
        else if (view.getId() == R.id.selfie) {
            Intent takeAPictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(takeAPictureIntent, TAKE_PICTURE);
        }
        else if (view.getId() == R.id.uploadPic){
            Intent uploadPicFromGallery = new Intent();
            uploadPicFromGallery.setType("image/*");
            uploadPicFromGallery.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(
                    Intent.createChooser(
                            uploadPicFromGallery, "Select Picture"), UPLOAD_PICTURE);
        }
        else if (view.getId() == R.id.McLovinMe && !userProvidedImage){
            moveToDisplay.putExtra("picture", mcLovinFaceUri);
            startActivity(moveToDisplay);
        }
        else if (view.getId() == R.id.McLovinMe && userProvidedImage) {
            startActivity(moveToDisplay);
        }
    }
}

